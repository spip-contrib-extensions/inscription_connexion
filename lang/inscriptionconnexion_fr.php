<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/inscription_connexion.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_compte_existant' => 'Un compte existe déjà avec cet adresse email. Connectez-vous avec ou bien utilisez une autre adresse.'
);
