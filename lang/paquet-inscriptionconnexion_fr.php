<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/inscription_connexion.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'inscriptionconnexion_description' => 'Ce plugin permet de connecter la personne dès que son inscription s’est correctement terminée. Il n’y a rien à faire, juste activer le plugin. Cela est utile dans les cas où le risque de fausse information est très faible, et où on veut fluidifier le parcours utilisateur, par exemple dans un tunnel de commande pour payer après.',
	'inscriptionconnexion_nom' => 'Connexion dès l’inscription',
	'inscriptionconnexion_slogan' => 'Connecte le visiteur dès la fin de son inscription.'
);
